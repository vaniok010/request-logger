# Laravel Request Logger

[![Latest Version on Packagist](https://img.shields.io/packagist/v/hryha/request-logger.svg)](https://packagist.org/packages/hryha/request-logger)
[![Total Downloads](https://img.shields.io/packagist/dt/hryha/request-logger.svg)](https://packagist.org/packages/hryha/request-logger)
[![PHP Version](https://img.shields.io/badge/PHP-7.3%2B-blue)](https://www.php.net/)
[![Laravel Version](https://img.shields.io/badge/Laravel-5.7%2B-brightgreen)](https://laravel.com/)
[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg)](LICENSE.md)

![Laravel request logger](assets/demo.png)

## Overview

This package provides middleware that logs incoming HTTP requests and responses in Laravel/Lumen applications.
You can view your logs through a dedicated panel at `https://your.domain/request-logs`.

## Features

- HTTP request and response logging
- Web-based log viewer interface
- Configurable data retention period
- Sensitive data masking
- Support for custom logging fields
- Multiple storage drivers (File system, MySQL)

## Requirements

- PHP 7.3 or higher
- Laravel/Lumen 5.7 or higher
- MySQL 5.7 or higher (required for database driver)

## Laravel Installation

Install the package via Composer::

```bash
composer require hryha/request-logger
```

Run the database migrations:

```php
    php artisan migrate
```

Optionally, publish the package's configuration file:

```bash
php artisan vendor:publish --provider="Hryha\RequestLogger\RequestLoggerServiceProvider" 
```

View the complete configuration options: [here](config/request-logger.php).

The package provides middleware that can be registered either globally or on specific routes.

```php
// In app/Http/Kernel.php for global middleware
protected $middleware = [
    // ...
    \Hryha\RequestLogger\Http\Middleware\RequestLogger::class
];
```

```php
// Or in your routes file for specific routes
Route::get('/user', [UserController::class, 'index'])->middleware(\Hryha\RequestLogger\Http\Middleware\RequestLogger::class);
```

## Lumen Installation

Install the package via Composer:

```bash
composer require hryha/request-logger
```

First, install the [vendor:publish plugin](https://github.com/laravelista/lumen-vendor-publish).

Register the service provider:

```php
//In 'bootstrap/app.php'
$app->register(\Hryha\RequestLogger\RequestLoggerServiceProvider::class); 
```

Optionally, publish the configuration file:

```bash
php artisan vendor:publish --provider="Hryha\RequestLogger\RequestLoggerServiceProvider" 
```

If you publish the configuration, register it in your application:

```php
// In 'bootstrap/app.php'
$app->configure('request-logger');
```

Run the database migrations:

```bash
php artisan migrate
```

Register the request logger middleware:

```php
// In 'bootstrap/app.php'
$app->routeMiddleware([
    // ...
    'request-logger' => \Hryha\RequestLogger\Http\Middleware\RequestLogger::class,
]);
```

Use the middleware on specific routes:

```php
// In your routes file
Route::post('/test', ['uses' => 'TestController@test', 'middleware' => ['request-logger']]);
```

## Data Pruning

To prevent the `request_logs` table from growing too large, schedule the `request-logs:clear` command to run daily:

```php
$schedule->command('request-logs:clear')->daily();
```

The `request-logs:clear` command removes logs older than the number of days specified in your `log_keep_days`
configuration.
To delete all logs, use the --all parameter:

```bash
php artisan request-logs:clear --all
```

## Custom Fields

The Request Logger supports additional custom fields for enhanced logging capabilities.

Use the `RequestLogger::addCustomField(key, value)` method to include additional data in your logs.
Ensure that any custom fields are properly defined in the `request_logs` database table.

### Implementation Example

Follow these steps to implement custom fields:

1. Create a middleware class (e.g., `ConfigureRequestLogger`)
2. Implement the `handle` method using `RequestLogger::addCustomField(key, value)`
3. Register the middleware in your `Http\Kernel.php` file to activate it.

Example middleware implementation:

```php
<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Hryha\RequestLogger\RequestLogger;
use Illuminate\Http\Request;

final class ConfigureRequestLogger
{
    private RequestLogger $requestLogger;

    public function __construct(RequestLogger $requestLogger)
    {
        $this->requestLogger = $requestLogger;
    }

    public function handle(Request $request, Closure $next)
    {
        if (config('request-logger.enabled')) {
            /** @var User|null $user */
            $user = $request->user();

            // Log the authenticated user's ID
            $this->requestLogger->addCustomField('user_id', $user?->id);
        }

        return $next($request);
    }
}
```

## Ignoring Responses by Status Code

Configure status codes to ignore by setting `REQUEST_LOGGER_IGNORE_RESPONSE_STATUSES` in your `.env` file.
The setting accepts both status ranges and specific status codes:

```dotenv
REQUEST_LOGGER_IGNORE_RESPONSE_STATUSES="[[100, 299], 301, 302]"
```

This configuration will ignore logs for responses with status codes between `100-299`, as well as `301` and `302`
responses.

## Testing

Run the test:

``` bash
php vendor/bin/phpunit
```

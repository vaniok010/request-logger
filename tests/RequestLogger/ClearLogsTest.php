<?php

namespace Hryha\RequestLogger\Tests\RequestLogger;

use Carbon\Carbon;
use Hryha\RequestLogger\Models\RequestLog;
use Hryha\RequestLogger\Models\RequestLogFingerprint;
use Hryha\RequestLogger\Services\ClearLogs;
use Hryha\RequestLogger\Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class ClearLogsTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    protected $requestLogFingerprint;
    protected $oldRequestLog;
    protected $newRequestLog;
    protected $newLogFilePath;
    protected $oldLogFilePath;

    protected function setUp(): void
    {
        parent::setUp();

        Storage::fake();

        Config::set('request-logger.log_keep_days', 1);

        $newDate = Carbon::now();
        $oldDate = Carbon::now()->subDays(2);

        $this->requestLogFingerprint = RequestLogFingerprint::query()->create([
            'fingerprint' => $this->faker->uuid,
            'repeating' => rand(1, 1000),
        ]);

        $this->newLogFilePath = implode([
            'request-logs/',
            $newDate->format('Y-m-d'),
            '/',
            $newDate->format('H'),
            'h-',
            $newDate->format('m'),
            'm/test.log'
        ]);

        $this->oldLogFilePath = implode([
            'request-logs/',
            $oldDate->format('Y-m-d'),
            '/',
            $oldDate->format('H'),
            'h-',
            $oldDate->format('m'),
            'm/test.log'
        ]);

        Storage::put($this->newLogFilePath, '');
        Storage::put($this->oldLogFilePath, '');

        $this->oldRequestLog = RequestLog::query()->create([
            'fingerprint_id' => $this->requestLogFingerprint->id,
            'user_id' => 1,
            'method' => $this->faker->randomElement(['GET', 'POST', 'PUT', 'DELETE']),
            'host' => $this->faker->domainName(),
            'url' => $this->faker->url(),
            'response_status_code' => rand(200, 500),
            'duration_ms' => rand(1, 1000),
            'memory' => rand(1, 1000),
            'ip' => $this->faker->ipv4(),
            'date' => $oldDate,
            'log_file' => $this->oldLogFilePath,
        ]);

        $this->newRequestLog = $this->oldRequestLog->replicate();
        $this->newRequestLog->date = $newDate;
        $this->newRequestLog->log_file = $this->newLogFilePath;
        $this->newRequestLog->save();
    }

    public function test_all_logs_can_be_deleted(): void
    {
        $this->assertTrue(Storage::exists($this->oldLogFilePath), 'Old file does not exist');
        $this->assertTrue(Storage::exists($this->newLogFilePath), 'New file does not exist');

        $this->assertDatabaseCount(RequestLog::class, 2);
        $this->assertDatabaseCount(RequestLogFingerprint::class, 1);

        ($this->app->make(ClearLogs::class))->all();

        $this->assertDatabaseCount(RequestLog::class, 0);
        $this->assertDatabaseCount(RequestLogFingerprint::class, 0);

        $this->assertFalse(Storage::exists($this->oldLogFilePath), 'Old dile exist');
        $this->assertFalse(Storage::exists($this->newLogFilePath), 'New file exist');
    }

    /**
     * @throws \Exception
     */
    public function test_old_logs_can_be_deleted(): void
    {
        $this->assertTrue(Storage::exists($this->oldLogFilePath), 'Old file does not exist');
        $this->assertTrue(Storage::exists($this->newLogFilePath), 'New file does not exist');

        $this->assertDatabaseCount(RequestLog::class, 2);
        $this->assertDatabaseCount(RequestLogFingerprint::class, 1);

        ($this->app->make(ClearLogs::class))->old();

        $this->assertDatabaseCount(RequestLogFingerprint::class, 1);

        $this->assertDatabaseHas(RequestLog::class, [
            'id' => $this->newRequestLog->id,
        ]);

        $this->assertDatabaseMissing(RequestLog::class, [
            'id' => $this->oldRequestLog->id,
        ]);

        $this->assertFalse(Storage::exists($this->oldLogFilePath), 'Old dile exist');
        $this->assertTrue(Storage::exists($this->newLogFilePath), 'New file exist');
    }
}

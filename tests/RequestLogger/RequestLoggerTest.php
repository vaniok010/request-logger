<?php

namespace Hryha\RequestLogger\Tests\RequestLogger;

use Hryha\RequestLogger\Models\RequestLog;
use Hryha\RequestLogger\RequestLogger;
use Hryha\RequestLogger\Services\ResponseFormatter;
use Hryha\RequestLogger\Support\Helpers\FileHelper;
use Hryha\RequestLogger\Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Orchestra\Testbench\Factories\UserFactory;

class RequestLoggerTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;

    public function test_basic_check_for_save_logs(): void
    {
        $request = Request::create('/test', 'post', [], [], [], [], 'test');
        $response = new Response('test', 200);

        $fileHelper = $this->app->make(FileHelper::class);
        /** @var RequestLogger $requestLogger */
        $requestLogger = $this->app->make(RequestLogger::class);
        $requestLogger->save($request, $response);

        $data = ['method' => $request->getMethod(), 'url' => $request->getRequestUri(), 'host' => $request->getHost()];
        $this->assertDatabaseHas(RequestLog::class, $data);
        $this->assertTrue(Storage::exists($fileHelper->getLogPath($requestLogger->logInfo)));
    }

    public function test_check_for_save_logs(): void
    {
        Storage::fake();

        $request = Request::create('/test', 'post', [], [], [], [], 'test');
        $response = new Response('test', 200);
        $total_request_logs = RequestLog::query()->count();

        $total = $this->faker->numberBetween(10, 100);

        /** @var FileHelper $file_helper */
        $file_helper = $this->app->make(FileHelper::class);
        /** @var RequestLogger $requestLogger */
        $requestLogger = $this->app->make(RequestLogger::class);

        for ($i = 0; $i < $total; $i++) {
            $requestLogger->save($request, $response);
        }

        $this->assertEquals($total_request_logs + $total, RequestLog::query()->count());

        $folderLogs = $file_helper->getFolderLogs($requestLogger->logInfo);

        $files = Storage::files($folderLogs);

        $this->assertGreaterThanOrEqual(count($files), $total);
    }

    public function test_check_user_id_save_logs(): void
    {
        $request = Request::create('/test', 'post', [], [], [], [], 'test');
        $response = new Response('test', 200);

        $user = UserFactory::new()->make(['id' => 999]);

        /** @var FileHelper $fileHelper */
        $fileHelper = $this->app->make(FileHelper::class);
        /** @var RequestLogger $requestLogger */
        $requestLogger = $this->app->make(RequestLogger::class);
        $requestLogger->addCustomField('user_id', $user->id);

        $requestLogger->save($request, $response);

        $fileContent = Storage::get($fileHelper->getLogPath($requestLogger->logInfo));

        $result = json_decode($fileContent, true);

        $resultResponseContent = Arr::get($result, 'response.content');

        $this->assertEquals(ResponseFormatter::$defaultContent, $resultResponseContent);
    }

    public function test_check_save_logs_with_different_url_types(): void
    {
        $this->withoutMix();

        $response = new Response('test', 200);
        $total_request_logs = RequestLog::query()->count();

        $requests = collect();

        $total_entries = $this->faker->numberBetween(10, 100);

        /** @var RequestLogger $request_logger */
        $request_logger = $this->app->make(RequestLogger::class);

        for ($i = 0; $i < $total_entries; $i++) {
            $method = $this->faker->randomElement([Request::METHOD_GET, Request::METHOD_POST]);
            $url = $this->faker->url();
            $urlWithoutDomain = preg_replace('#^.+://[^/]+#', '', $url);

            $request = Request::create($this->faker->randomElement([$url, $urlWithoutDomain]), $method, [], [], [], [], $this->faker->sentence());
            $requests->push($request);
        }

        foreach ($requests as $request) {
            $request_logger->save($request, $response);
        }

        $this->assertEquals($total_request_logs + $total_entries, RequestLog::query()->count());
        $this->call('GET', '/request-logs', [], [], [], $this->getServerBasicAuthParams())->assertStatus(200);
    }

    public function test_do_not_save_logs_when_disabled(): void
    {
        $request = Request::create('/test', 'post', [], [], [], [], 'test');
        $response = new Response('test', 200);

        $total_request_logs = RequestLog::query()->count();

        $this->app['config']->set('request-logger.enabled', false);

        /** @var RequestLogger $request_logger */
        $request_logger = $this->app->make(RequestLogger::class);
        $request_logger->save($request, $response);

        $this->assertEquals($total_request_logs, RequestLog::query()->count());
    }

    public function test_do_not_save_logs_when_path_is_ignored(): void
    {
        $request = Request::create('/test', 'post', [], [], [], [], 'test');
        $response = new Response('test', 200);

        $total_request_logs = RequestLog::query()->count();

        $this->app['config']->set('request-logger.ignore_paths', ['test*']);

        /** @var RequestLogger $request_logger */
        $request_logger = $this->app->make(RequestLogger::class);
        $request_logger->save($request, $response);

        $this->assertEquals($total_request_logs, RequestLog::query()->count());
    }

    public function test_do_not_save_logs_when_status_is_ignored(): void
    {
        $request = Request::create('/test', 'post', [], [], [], [], 'test');
        $first_response = new Response('test', 200);
        $second_response = new Response('test', 400);

        $total_request_logs = RequestLog::query()->count();

        $this->app['config']->set('request-logger.ignore_response_statuses', [[100,300], 400]);

        /** @var RequestLogger $request_logger */
        $request_logger = $this->app->make(RequestLogger::class);
        $request_logger->save($request, $first_response);
        $request_logger->save($request, $second_response);

        $this->assertEquals($total_request_logs, RequestLog::query()->count());
    }

    public function test_do_not_save_self_logs(): void
    {
        $request = Request::create('/request-logs', 'post', [], [], [], [], 'test');
        $response = new Response();

        $total_request_logs = RequestLog::query()->count();

        $this->app['config']->set('request-logger.ignore_paths', '');

        /** @var RequestLogger $request_logger */
        $request_logger = $this->app->make(RequestLogger::class);
        $request_logger->save($request, $response);

        $this->assertEquals($total_request_logs, RequestLog::query()->count());
    }
}

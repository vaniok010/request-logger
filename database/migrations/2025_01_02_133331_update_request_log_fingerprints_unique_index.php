<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateRequestLogFingerprintsUniqueIndex extends Migration
{
    public function up(): void
    {
        Schema::table(config('request-logger.table_name') . '_fingerprints', function (Blueprint $table) {
            $table->dropIndex(['fingerprint']);
            $table->unique(['fingerprint']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table(config('request-logger.table_name') . '_fingerprints', function (Blueprint $table) {
            $table->index(['fingerprint']);
            $table->dropUnique(['fingerprint']);
        });
    }
}

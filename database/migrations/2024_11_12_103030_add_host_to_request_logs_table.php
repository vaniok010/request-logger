<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddHostToRequestLogsTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table(config('request-logger.table_name'), function (Blueprint $table) {
            $table->string('host')->after('method')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table(config('request-logger.table_name'), function (Blueprint $table) {
            $table->dropColumn('host');
        });
    }
}

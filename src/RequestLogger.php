<?php

namespace Hryha\RequestLogger;

use DateTime;
use DateTimeZone;
use Hryha\RequestLogger\Writers\LogWriter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

final class RequestLogger
{
    /**
     * @var LogInfo $logInfo
     */
    public $logInfo;

    /**
     * @var LogWriter $logWriter
     */
    protected $logWriter;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var array
     */
    private $customFields = [];

    /**
     * @param LogWriter $logWriter
     */
    public function __construct(LogWriter $logWriter)
    {
        $this->logWriter = $logWriter;
    }

    /**
     * @param Request $request
     * @param Response $response
     */
    public function save(Request $request, Response $response)
    {
        if (!config('request-logger.enabled') || $this->shouldIgnore($request, $response)) {
            return;
        }

        $this->request = $request;

        $customFields = $this->getCustomFields();

        $this->logInfo = new LogInfo(
            $request,
            $response,
            $this->getLoggerStart(),
            $this->getDuration(),
            $this->getMemoryUsage(),
            $this->getFingerprint(),
            $customFields
        );

        $this->logWriter->write($this->logInfo);
    }

    /**
     * @return float
     */
    protected function getStartTime(): float
    {
        $start_time = defined('LARAVEL_START') ? LARAVEL_START : (float)$this->request->server('REQUEST_TIME_FLOAT');
        if (!strpos($start_time, '.')) {
            $start_time .= '.0001';
        }
        return (float)$start_time;
    }

    /**
     * @return DateTime
     */
    protected function getLoggerStart(): DateTime
    {
        $start_time = $this->getStartTime();
        $logger_start = DateTime::createFromFormat('U.u', $start_time);

        try {
            $timezone = new DateTimeZone(config('request-logger.timezone'));
            $logger_start->setTimezone($timezone);
        } catch (Throwable $e) {
            Log::error($e);
        }

        return $logger_start;
    }

    /**
     * @return float
     */
    protected function getDuration(): float
    {
        $start_time = $this->getStartTime();
        return round((microtime(true) - $start_time) * 1000);
    }

    /**
     * @return float
     */
    protected function getMemoryUsage(): float
    {
        return round(memory_get_peak_usage(true) / 1024 / 1024, 1);
    }

    /**
     * @return string
     */
    public function getFingerprint(): string
    {
        return sha1(implode('|', [
            $this->request->method(),
            $this->request->fullUrl(),
            $this->request->getContent(),
        ]));
    }

    /**
     * Determine if the log should be ignored.
     *
     * @param Request $request
     * @param Response $response
     * @return bool
     */
    private function shouldIgnore(Request $request, Response $response): bool
    {
        if ($this->shouldIgnorePath($request)) {
            return true;
        }

        return $this->shouldIgnoreResponseStatus($response);
    }

    /**
     * Determine if the log should be ignored by the request path.
     *
     * @param Request $request
     * @return bool
     */
    private function shouldIgnorePath(Request $request): bool
    {
        $ignorePaths = config('request-logger.ignore_paths');
        $ignorePaths = is_array($ignorePaths) ? $ignorePaths : [];
        $ignorePaths = array_unique(array_merge($ignorePaths, ['request-logs*']));

        foreach ($ignorePaths as $ignorePath) {
            if ($request->is($ignorePath)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine if the log should be ignored by the response status.
     *
     * @param Response $response
     * @return bool
     */
    private function shouldIgnoreResponseStatus(Response $response): bool
    {
        $ignoreStatuses = config('request-logger.ignore_response_statuses', []);
        foreach ($ignoreStatuses as $ignoreStatus) {
            if ($response->getStatusCode() === $ignoreStatus) {
                return true;
            }
            if (is_array($ignoreStatus) && 2 === count($ignoreStatus)) {
                if ($response->getStatusCode() >= $ignoreStatus[0] && $response->getStatusCode() <= $ignoreStatus[1]) {
                    return true;
                }
            }
        }

        return false;
    }

    protected function getCustomFields(): array
    {
        return $this->customFields;
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function addCustomField(string $key, $value): void
    {
        $this->customFields[$key] = $value;
    }
}

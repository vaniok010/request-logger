<?php

namespace Hryha\RequestLogger\Writers;

use Hryha\RequestLogger\LogInfo;
use Hryha\RequestLogger\Models\RequestLog;
use Hryha\RequestLogger\Models\RequestLogFingerprint;
use Hryha\RequestLogger\Support\Helpers\FileHelper;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Throwable;

class DbWriter implements Writer
{
    /**
     * @var FileHelper $fileHelper
     */
    protected $fileHelper;

    /**
     * @param FileHelper $fileHelper
     */
    public function __construct(FileHelper $fileHelper)
    {
        $this->fileHelper = $fileHelper;
    }

    public function write(LogInfo $logInfo)
    {
        try {
            $requestLogFingerprintValues = [
                'fingerprint' => $logInfo->fingerprint,
                'repeating' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ];

            $requestLogFingerprintTable = (new RequestLogFingerprint)->getTable();
            $querySql = "insert `$requestLogFingerprintTable` (fingerprint, repeating, created_at, updated_at) values (:fingerprint, :repeating, :created_at, :updated_at) ON DUPLICATE KEY UPDATE `repeating` = `repeating` + 1, `updated_at` = values(`updated_at`)";
            DB::insert($querySql, $requestLogFingerprintValues);

            $fingerprint = RequestLogFingerprint::query()->where('fingerprint', $logInfo->fingerprint)->first();

            RequestLog::query()->create(array_merge([
                'fingerprint_id' => $fingerprint->id,
                'method' => $logInfo->request->getMethod(),
                'host' => Str::limit($logInfo->request->getHost(), 250),
                'url' => Str::limit($logInfo->request->getRequestUri(), 250),
                'ip' => $logInfo->request->getClientIp(),
                'log_file' => $this->fileHelper->getLogPath($logInfo),
                'date' => $logInfo->loggerStart->format('Y-m-d H:i:s.u'),
                'response_status_code' => $logInfo->response->getStatusCode(),
                'duration_ms' => $logInfo->durationMs,
                'memory' => $logInfo->memoryUsage,
            ], $logInfo->customFields));

        } catch (Throwable $e) {
            Log::error($e);
        }
    }
}

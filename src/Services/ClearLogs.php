<?php

namespace Hryha\RequestLogger\Services;

use DateInterval;
use DateTime;
use Exception;
use Hryha\RequestLogger\Models\RequestLog;
use Hryha\RequestLogger\Models\RequestLogFingerprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ClearLogs
{
    private $keepDays;
    private $now;
    private $lastDate;

    public function __construct()
    {
        $this->keepDays = config('request-logger.log_keep_days');
        $this->now = new DateTime();
    }

    public function all()
    {
        $this->lastDate = (clone $this->now);
        $this->clearAllLogsFromDatabase();
        $this->clearLogsFromStorage(true);
    }

    /**
     * @throws Exception
     */
    public function old()
    {
        $this->lastDate = (clone $this->now)
            ->sub(new DateInterval('P' . ($this->keepDays - 1) . 'D'))
            ->setTime(00, 00, 00, 000000);
        $this->clearOldLogsFromDatabase();
        $this->clearLogsFromStorage();
    }

    /**
     * @throws Exception
     */
    private function clearOldLogsFromDatabase()
    {
        $logsToDelete = RequestLog::query()
            ->where('date', '<', $this->lastDate)
            ->pluck('fingerprint_id', 'id')
            ->toArray();

        if (empty($logsToDelete)) {
            return;
        }

        $lastId = max(array_keys($logsToDelete));

        RequestLog::query()
            ->where('id', '<=', $lastId)
            ->delete();

        $numberOfRepetitions = array_count_values($logsToDelete);

        $fingerprintRepeats = [];
        foreach ($numberOfRepetitions as $fingerprintId => $repeats) {
            $fingerprintRepeats[$repeats][] = $fingerprintId;
        }

        foreach ($fingerprintRepeats as $repeats => $fingerprintIds) {
            $fingerprintChunks = array_chunk($fingerprintIds, 3000);
            foreach ($fingerprintChunks as $ids) {
                RequestLogFingerprint::query()
                    ->whereIn('id', $ids)
                    ->decrement('repeating', $repeats);
            }
        }

        RequestLogFingerprint::query()
            ->where('repeating', 0)
            ->delete();
    }

    private function clearAllLogsFromDatabase()
    {
        RequestLog::query()->delete();
        RequestLogFingerprint::query()->delete();
    }

    /**
     * @param false $all
     */
    private function clearLogsFromStorage($all = false)
    {
        $dirs = Storage::directories('request-logs');
        foreach ($dirs as $dir) {
            list(, $dateDir) = explode('/', $dir);
            $date = DateTime::createFromFormat('Y-m-d', $dateDir);
            if ($date !== false) {
                $date = $date->setTime(00, 00, 00, 000000);
                $interval = $date->diff($this->now);
                if ($interval->days >= $this->keepDays || $all) {
                    if ($interval->days === 0) {
                        $timeDirs = Storage::directories($dir);
                        foreach ($timeDirs as $timeDir) {
                            list(, , $hourMinuteDir) = explode('/', $timeDir);
                            list($hour, $minute) = explode('-', $hourMinuteDir);
                            $hour = filter_var($hour, FILTER_SANITIZE_NUMBER_INT);
                            $minute = filter_var($minute, FILTER_SANITIZE_NUMBER_INT);
                            $nowHour = $this->now->format('H');
                            $nowMinute = $this->now->format('i');
                            if ($hour < $nowHour || ($hour === $nowHour && $minute < $nowMinute)) {
                                Storage::deleteDirectory($timeDir);
                            }
                        }
                    } else {
                        Storage::deleteDirectory($dir);
                    }
                }
            }
        }
    }
}
